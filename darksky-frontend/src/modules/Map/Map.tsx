import Spinner from 'react-bootstrap/Spinner';
import Alert from 'react-bootstrap/Alert';
import 'maplibre-gl/dist/maplibre-gl.css';
import { useLatestLightMeasurementsOfLast24h } from '../../api/hooks';
import MapRenderer from './MapRenderer';

export default function Map() {
  const { isLoading, isError, data, error } =
    useLatestLightMeasurementsOfLast24h();

  if (isLoading) {
    return (
      <Spinner animation="border" role="status">
        <span className="visually-hidden">Loading...</span>
      </Spinner>
    );
  }

  if (isError) {
    return (
      <Alert variant="danger">
        {error instanceof Error ? error.message : 'An error occurred'}
      </Alert>
    );
  }

  return <MapRenderer data={data} />;
}
