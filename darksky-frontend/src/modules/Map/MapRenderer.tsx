import { useEffect, useRef } from 'react';
import maplibregl from 'maplibre-gl';

import { LightMeasurement } from '../../api/data-definitions';

type MapRendererProps = {
  data: LightMeasurement[];
};

export default function MapRenderer({ data }: MapRendererProps) {
  const mapContainer = useRef<HTMLDivElement>(null);
  const map = useRef<maplibregl.Map | null>(null);

  // Instantiate the map
  useEffect(() => {
    if (mapContainer.current) {
      map.current = new maplibregl.Map({
        container: mapContainer.current,
        style: 'https://demotiles.maplibre.org/style.json', // MapLibre open style URL
        center: [0, 0], // Center coordinates
        zoom: 1,
      });
    }

    return () => {
      map.current?.remove();
    };
  }, []);

  // Render the measurements as hoverable markers on the map
  useEffect(() => {
    if (map.current && data && data.length > 0) {
      data.forEach((measurement: LightMeasurement) => {
        const marker = new maplibregl.Marker()
          .setLngLat([measurement.lon, measurement.lat])
          .setPopup(
            new maplibregl.Popup({ offset: 25 }).setHTML(
              `<pre>${JSON.stringify(measurement, null, 2)}</pre>`
            )
          )
          // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
          .addTo(map.current!);

        marker.getElement().addEventListener('mouseover', () => {
          // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
          map.current!.getCanvas().style.cursor = 'context-menu';
        });

        marker.getElement().addEventListener('mouseleave', () => {
          // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
          map.current!.getCanvas().style.cursor = 'grab';
        });
      });
    }
  }, [data]);

  return <div ref={mapContainer} style={{ width: '100%', height: '100vh' }} />;
}
