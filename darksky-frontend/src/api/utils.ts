export default function getLast24hQueryString(): string {
  const now = new Date();
  const yesterday = new Date(now);
  yesterday.setHours(now.getHours() - 24);

  return `min_date=${yesterday.toISOString()}`;
}
