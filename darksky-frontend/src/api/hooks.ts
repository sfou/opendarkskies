import { useQuery } from '@tanstack/react-query';
import { useMemo } from 'react';
import { LightMeasurement } from './data-definitions';
import { LIGHT_MEASUREMENTS_ENDPOINT as LM_ENDPOINT } from './api-settings';

async function fetchLightMeasurements(
  query?: string
): Promise<LightMeasurement[]> {
  const res = await fetch(query ? `${LM_ENDPOINT}?${query}` : `${LM_ENDPOINT}`);
  if (!res.ok) {
    throw new Error('Network error.');
  }
  return res.json() as Promise<LightMeasurement[]>;
}

export function useLightMeasurements(query?: string) {
  return useQuery<LightMeasurement[]>({
    queryKey: ['lightMeasurements', query],
    queryFn: () => fetchLightMeasurements(query),
  });
}

export function useLatestLightMeasurementsOfLast24h() {
  const queryString = useMemo(() => {
    const now = new Date();
    const oneDayBefore = new Date();
    oneDayBefore.setDate(oneDayBefore.getDate() - 1);

    return `min_date=${oneDayBefore.toISOString()}&max_date=${now.toISOString()}&only_latest=true`;
  }, []);

  return useQuery<LightMeasurement[]>({
    queryKey: ['lightMeasurements', queryString],
    queryFn: () => fetchLightMeasurements(queryString),
  });
}

async function fetchLightMeasurement(id: number): Promise<LightMeasurement> {
  const res = await fetch(`/api/light-measurements/${id}`);
  if (!res.ok) {
    throw new Error('Network response was not ok');
  }
  return res.json() as Promise<LightMeasurement>;
}

export function useLightMeasurement(id: number) {
  return useQuery<LightMeasurement>({
    queryKey: ['lightMeasurement', id],
    queryFn: () => fetchLightMeasurement(id),
  });
}
