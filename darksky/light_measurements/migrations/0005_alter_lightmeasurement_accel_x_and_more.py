# Generated by Django 4.2.6 on 2024-01-22 10:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('light_measurements', '0004_lightmeasurement_sensor_id'),
    ]

    operations = [
        migrations.AlterField(
            model_name='lightmeasurement',
            name='accel_x',
            field=models.FloatField(blank=True, help_text='Acceleration on X axis', null=True),
        ),
        migrations.AlterField(
            model_name='lightmeasurement',
            name='accel_x_sd',
            field=models.FloatField(blank=True, help_text='Standard deviation of acceleration on X axis', null=True),
        ),
        migrations.AlterField(
            model_name='lightmeasurement',
            name='accel_y',
            field=models.FloatField(blank=True, help_text='Acceleration on Y axis', null=True),
        ),
        migrations.AlterField(
            model_name='lightmeasurement',
            name='accel_y_sd',
            field=models.FloatField(blank=True, help_text='Standard deviation of acceleration on Y axis', null=True),
        ),
        migrations.AlterField(
            model_name='lightmeasurement',
            name='accel_z',
            field=models.FloatField(blank=True, help_text='Acceleration on Z axis', null=True),
        ),
        migrations.AlterField(
            model_name='lightmeasurement',
            name='accel_z_sd',
            field=models.FloatField(blank=True, help_text='Standard deviation of acceleration on Z axis', null=True),
        ),
        migrations.AlterField(
            model_name='lightmeasurement',
            name='battery',
            field=models.FloatField(blank=True, help_text='Battery voltage', null=True),
        ),
        migrations.AlterField(
            model_name='lightmeasurement',
            name='humidity',
            field=models.FloatField(blank=True, help_text='Relative Humidity', null=True),
        ),
        migrations.AlterField(
            model_name='lightmeasurement',
            name='humidity_sd',
            field=models.FloatField(blank=True, help_text='Relative Humidity (Standard deviation)', null=True),
        ),
        migrations.AlterField(
            model_name='lightmeasurement',
            name='lux_1',
            field=models.FloatField(blank=True, help_text='Light on channel 1', null=True),
        ),
        migrations.AlterField(
            model_name='lightmeasurement',
            name='lux_1_sd',
            field=models.FloatField(blank=True, help_text='Light on channel 1 (Standard deviation)', null=True),
        ),
        migrations.AlterField(
            model_name='lightmeasurement',
            name='lux_2',
            field=models.FloatField(blank=True, help_text='Light on channel 2', null=True),
        ),
        migrations.AlterField(
            model_name='lightmeasurement',
            name='lux_2_sd',
            field=models.FloatField(blank=True, help_text='Light on channel 2 (Standard deviation)', null=True),
        ),
        migrations.AlterField(
            model_name='lightmeasurement',
            name='lux_calibrated',
            field=models.FloatField(blank=True, help_text='Calibrated value of light', null=True),
        ),
        migrations.AlterField(
            model_name='lightmeasurement',
            name='lux_calibrated_sd',
            field=models.FloatField(blank=True, help_text='Calibrated value of light (Standard deviation)', null=True),
        ),
        migrations.AlterField(
            model_name='lightmeasurement',
            name='pressure',
            field=models.FloatField(blank=True, help_text='Pressure', null=True),
        ),
        migrations.AlterField(
            model_name='lightmeasurement',
            name='pressure_sd',
            field=models.FloatField(blank=True, help_text='Pressure (Standard deviation)', null=True),
        ),
        migrations.AlterField(
            model_name='lightmeasurement',
            name='temp',
            field=models.FloatField(blank=True, help_text='Temperature in degrees Celcius', null=True),
        ),
        migrations.AlterField(
            model_name='lightmeasurement',
            name='temp_sd',
            field=models.FloatField(blank=True, help_text='Temperature (Standard deviation)', null=True),
        ),
    ]
