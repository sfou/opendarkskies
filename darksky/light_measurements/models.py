# pylint: disable=C0116

from django.core.exceptions import ValidationError
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models


class LightMeasurement(models.Model):  # pylint: disable=R0902
    """Represents a single light measurement"""

    sensor_id = models.CharField(max_length=12, db_index=True)
    timestamp = models.DateTimeField(db_index=True)
    alt = models.PositiveIntegerField(blank=True, null=True, help_text="In meters above sea level")
    lat = models.FloatField(
        validators=[MaxValueValidator(90), MinValueValidator(-90)],
        help_text="Latitude, eg. 38.01697",
    )
    lon = models.FloatField(
        validators=[MaxValueValidator(180), MinValueValidator(-180)],
        help_text="Longitude, eg. 23.7314",
    )
    battery = models.FloatField(blank=True, null=True, help_text="Battery voltage")

    accel_x = models.FloatField(blank=True, null=True, help_text="Acceleration on X axis")
    accel_y = models.FloatField(blank=True, null=True, help_text="Acceleration on Y axis")
    accel_z = models.FloatField(blank=True, null=True, help_text="Acceleration on Z axis")

    @property
    def accel(self):
        return [self.accel_x, self.accel_y, self.accel_z]

    @accel.setter
    def accel(self, accel_list):
        if not accel_list:
            self.accel_x, self.accel_y, self.accel_z = [None, None, None]
        elif len(accel_list) == 3:
            self.accel_x, self.accel_y, self.accel_z = accel_list
        else:
            raise ValidationError("'accel' must contain three values")

    accel_x_sd = models.FloatField(
        blank=True, null=True, help_text="Standard deviation of acceleration on X axis"
    )
    accel_y_sd = models.FloatField(
        blank=True, null=True, help_text="Standard deviation of acceleration on Y axis"
    )
    accel_z_sd = models.FloatField(
        blank=True, null=True, help_text="Standard deviation of acceleration on Z axis"
    )

    @property
    def accel_sd(self):
        return [self.accel_x_sd, self.accel_y_sd, self.accel_z_sd]

    @accel_sd.setter
    def accel_sd(self, accel_list):
        if not accel_list:
            self.accel_x_sd, self.accel_y_sd, self.accel_z_sd = [None, None, None]
        elif len(accel_list) == 3:
            self.accel_x_sd, self.accel_y_sd, self.accel_z_sd = accel_list
        else:
            raise ValidationError("'accel_sd' must contain three values")

    lux_1 = models.FloatField(blank=True, null=True, help_text="Light on channel 1")
    lux_2 = models.FloatField(blank=True, null=True, help_text="Light on channel 2")
    lux_calibrated = models.FloatField(blank=True, null=True, help_text="Calibrated value of light")

    @property
    def lux(self):
        return [self.lux_1, self.lux_2, self.lux_calibrated]

    @lux.setter
    def lux(self, lux_list):
        if not lux_list:
            self.lux_1, self.lux_2, self.lux_calibrated = [None, None, None]
        elif len(lux_list) == 3:
            self.lux_1, self.lux_2, self.lux_calibrated = lux_list
        else:
            raise ValidationError("'lux' must contain three values")

    lux_1_sd = models.FloatField(
        blank=True, null=True, help_text="Light on channel 1 (Standard deviation)"
    )
    lux_2_sd = models.FloatField(
        blank=True, null=True, help_text="Light on channel 2 (Standard deviation)"
    )
    lux_calibrated_sd = models.FloatField(
        blank=True,
        null=True,
        help_text="Calibrated value of light (Standard deviation)",
    )

    @property
    def lux_sd(self):
        return [self.lux_1, self.lux_2, self.lux_calibrated]

    @lux_sd.setter
    def lux_sd(self, lux_list):
        if not lux_list:
            self.lux_1_sd, self.lux_2_sd, self.lux_calibrated_sd = [None, None, None]
        elif lux_list and len(lux_list) == 3:
            self.lux_1_sd, self.lux_2_sd, self.lux_calibrated_sd = lux_list
        else:
            raise ValidationError("'lux_sd' must contain three values")

    temp = models.FloatField(blank=True, null=True, help_text="Temperature in degrees Celcius")
    humidity = models.FloatField(blank=True, null=True, help_text="Relative Humidity")
    pressure = models.FloatField(blank=True, null=True, help_text="Pressure")

    temp_sd = models.FloatField(blank=True, null=True, help_text="Temperature (Standard deviation)")
    humidity_sd = models.FloatField(
        blank=True, null=True, help_text="Relative Humidity (Standard deviation)"
    )
    pressure_sd = models.FloatField(
        blank=True, null=True, help_text="Pressure (Standard deviation)"
    )

    class Meta:
        ordering = ["-timestamp"]

    def __str__(self):
        return f"Light Measurement #{self.id} - Sensor ID: {self.sensor_id}"
