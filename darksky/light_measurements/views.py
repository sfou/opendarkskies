from django.db.models import OuterRef, Subquery
from rest_framework import viewsets

from .filters import LightMeasurementFilter
from .models import LightMeasurement
from .serializers import LightMeasurementSerializer


class LightMeasurementView(viewsets.ReadOnlyModelViewSet):  # pylint: disable=R0901
    """API view for listing and retrieving light measurements"""

    serializer_class = LightMeasurementSerializer
    filterset_class = LightMeasurementFilter

    def get_queryset(self):
        only_latest = self.request.query_params.get("only_latest", "false").lower() == "true"
        if only_latest:
            latest_per_sensor = (
                LightMeasurement.objects.filter(sensor_id=OuterRef("sensor_id")
                                                ).order_by("-timestamp").values("timestamp")[:1]
            )
            queryset = LightMeasurement.objects.filter(timestamp=Subquery(latest_per_sensor))
        else:
            queryset = LightMeasurement.objects.all()
        return queryset
