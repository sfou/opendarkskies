from django.apps import AppConfig


class LightMeasurementsConfig(AppConfig):
    """Darksky Light Measurements app config"""
    default_auto_field = "django.db.models.BigAutoField"
    name = "light_measurements"
