"""Define functions and settings for the django admin base interface"""
from django.contrib import admin
from light_measurements.models import LightMeasurement


@admin.register(LightMeasurement)
class SatelliteAdmin(admin.ModelAdmin):
    """Define LightMeasurement view in django admin UI"""
    list_display = (
        'id',
        'sensor_id',
        'timestamp',
        'alt',
        'lon',
        'lat',
        "accel_x",
        "accel_y",
        "accel_z",
        "lux_1",
        "lux_2",
        "lux_calibrated",
        "temp",
        "humidity",
        "pressure",
        "accel_x_sd",
        "accel_y_sd",
        "accel_z_sd",
        "lux_1_sd",
        "lux_2_sd",
        "lux_calibrated_sd",
        "temp_sd",
        "humidity_sd",
        "pressure_sd",
    )
    list_filter = ('id', 'sensor_id', 'timestamp', 'alt', 'lon', 'lat')
    search_fields = ('id', 'sensor_id')
