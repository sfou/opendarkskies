upstream django {
    server api:8000;
}

server {
    listen 80;
    server_name localhost;
    root   /var/lib/darksky/frontend-dist;
    index  index.html index.htm;

    location / {
        try_files $uri $uri/ =404;
    }

    location /api {
        proxy_pass http://django/api;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-Proto $scheme;
    }

    location /admin {
        proxy_pass http://django/admin;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-Proto $scheme;
    }

    location /static {
        alias /var/lib/darksky/staticfiles;

        expires 7d;
        add_header Cache-Control "public, max-age=604800, immutable";
    }
}